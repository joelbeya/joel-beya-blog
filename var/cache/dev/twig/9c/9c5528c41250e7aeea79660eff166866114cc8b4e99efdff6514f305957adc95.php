<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog/index.html.twig */
class __TwigTemplate_d1c692f97fe2144b66e4d247eead6003b46925467785e22377878f0316296ba2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "blog/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "BEYA Joel's blog!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<!-- Grid -->
<div class=\"w3-row\">

<!-- Blog entries -->
<div class=\"w3-col l8 s12\">
  <!-- Blog entry -->
  <div class=\"w3-card-4 w3-margin w3-white\">
  <img src=\"https://www.w3schools.com/w3images/woods.jpg\" alt=\"Nature\" style=\"width:100%\">
    <div class=\"w3-container\">
      <h3><b>TITLE HEADING</b></h3>
      <h5>Title description, <span class=\"w3-opacity\">April 7, 2014</span></h5>
    </div>

    <div class=\"w3-container\">
      <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed
        tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
      <div class=\"w3-row\">
        <div class=\"w3-col m8 s12\">
          <p><button class=\"w3-button w3-padding-large w3-white w3-border\">
            <b>READ MORE »</b>
          </button></p>
        </div>
        <div class=\"w3-col m4 w3-hide-small\">
          <p><span class=\"w3-padding-large w3-right\"><b>Comments  </b> <span class=\"w3-tag\">0</span></span></p>
        </div>
      </div>
    </div>
  </div>
  <hr>
</div>

<!-- Introduction menu -->
<div class=\"w3-col l4\">
<!-- About Card -->
<div class=\"w3-card w3-margin w3-margin-top\">
<img src=\"https://www.w3schools.com/w3images/avatar_g.jpg\" style=\"width:100%\">
<div class=\"w3-container w3-white\">
  <h4><b>My Name</b></h4>
  <p>Just me, myself and I, exploring the universe of uknownment. I have a heart of love and a interest of lorem ipsum and mauris neque quam blog. I want to share my world with you.</p>
</div>
</div><hr>

<!-- Posts -->
<div class=\"w3-card w3-margin\">
<div class=\"w3-container w3-padding\">
  <h4>Popular Posts</h4>
</div>
<ul class=\"w3-ul w3-hoverable w3-white\">
  <li class=\"w3-padding-16\">
    <img src=\"https://www.w3schools.com/w3images/workshop.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Lorem</span><br>
    <span>Sed mattis nunc</span>
  </li>
  <li class=\"w3-padding-16\">
    <img src=\"https://www.w3schools.com/w3images/gondol.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Ipsum</span><br>
    <span>Praes tinci sed</span>
  </li>
  <li class=\"w3-padding-16\">
    <img src=\"https://www.w3schools.com/w3images/skies.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Dorum</span><br>
    <span>Ultricies congue</span>
  </li>
  <li class=\"w3-padding-16 w3-hide-medium w3-hide-small\">
    <img src=\"https://www.w3schools.com/w3images/rock.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Mingsum</span><br>
    <span>Lorem ipsum dipsum</span>
  </li>
</ul>
</div>
<hr>

<!-- Labels / tags -->
<div class=\"w3-card w3-margin\">
<div class=\"w3-container w3-padding\">
  <h4>Tags</h4>
</div>
<div class=\"w3-container w3-white\">
<p><span class=\"w3-tag w3-black w3-margin-bottom\">Travel</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">New York</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">London</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">IKEA</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">NORWAY</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">DIY</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Ideas</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Baby</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Family</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">News</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Clothing</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Shopping</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Sports</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Games</span>
</p>
</div>
</div>

<!-- END Introduction Menu -->
</div>

<!-- END GRID -->
</div><br>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}BEYA Joel's blog!{% endblock %}

{% block body %}
<!-- Grid -->
<div class=\"w3-row\">

<!-- Blog entries -->
<div class=\"w3-col l8 s12\">
  <!-- Blog entry -->
  <div class=\"w3-card-4 w3-margin w3-white\">
  <img src=\"https://www.w3schools.com/w3images/woods.jpg\" alt=\"Nature\" style=\"width:100%\">
    <div class=\"w3-container\">
      <h3><b>TITLE HEADING</b></h3>
      <h5>Title description, <span class=\"w3-opacity\">April 7, 2014</span></h5>
    </div>

    <div class=\"w3-container\">
      <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed
        tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
      <div class=\"w3-row\">
        <div class=\"w3-col m8 s12\">
          <p><button class=\"w3-button w3-padding-large w3-white w3-border\">
            <b>READ MORE »</b>
          </button></p>
        </div>
        <div class=\"w3-col m4 w3-hide-small\">
          <p><span class=\"w3-padding-large w3-right\"><b>Comments  </b> <span class=\"w3-tag\">0</span></span></p>
        </div>
      </div>
    </div>
  </div>
  <hr>
</div>

<!-- Introduction menu -->
<div class=\"w3-col l4\">
<!-- About Card -->
<div class=\"w3-card w3-margin w3-margin-top\">
<img src=\"https://www.w3schools.com/w3images/avatar_g.jpg\" style=\"width:100%\">
<div class=\"w3-container w3-white\">
  <h4><b>My Name</b></h4>
  <p>Just me, myself and I, exploring the universe of uknownment. I have a heart of love and a interest of lorem ipsum and mauris neque quam blog. I want to share my world with you.</p>
</div>
</div><hr>

<!-- Posts -->
<div class=\"w3-card w3-margin\">
<div class=\"w3-container w3-padding\">
  <h4>Popular Posts</h4>
</div>
<ul class=\"w3-ul w3-hoverable w3-white\">
  <li class=\"w3-padding-16\">
    <img src=\"https://www.w3schools.com/w3images/workshop.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Lorem</span><br>
    <span>Sed mattis nunc</span>
  </li>
  <li class=\"w3-padding-16\">
    <img src=\"https://www.w3schools.com/w3images/gondol.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Ipsum</span><br>
    <span>Praes tinci sed</span>
  </li>
  <li class=\"w3-padding-16\">
    <img src=\"https://www.w3schools.com/w3images/skies.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Dorum</span><br>
    <span>Ultricies congue</span>
  </li>
  <li class=\"w3-padding-16 w3-hide-medium w3-hide-small\">
    <img src=\"https://www.w3schools.com/w3images/rock.jpg\" alt=\"Image\" class=\"w3-left w3-margin-right\" style=\"width:50px\">
    <span class=\"w3-large\">Mingsum</span><br>
    <span>Lorem ipsum dipsum</span>
  </li>
</ul>
</div>
<hr>

<!-- Labels / tags -->
<div class=\"w3-card w3-margin\">
<div class=\"w3-container w3-padding\">
  <h4>Tags</h4>
</div>
<div class=\"w3-container w3-white\">
<p><span class=\"w3-tag w3-black w3-margin-bottom\">Travel</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">New York</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">London</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">IKEA</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">NORWAY</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">DIY</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Ideas</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Baby</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Family</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">News</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Clothing</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Shopping</span>
  <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Sports</span> <span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">Games</span>
</p>
</div>
</div>

<!-- END Introduction Menu -->
</div>

<!-- END GRID -->
</div><br>
{% endblock %}
", "blog/index.html.twig", "/var/www/public/joel-beya-blog/templates/blog/index.html.twig");
    }
}
