<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_7ede54c38c324fef9c84f57cfa0dbef8ac324e9382b62dad28456b6953db062b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 12
        echo "        <style>
          body,h1,h2,h3,h4,h5 {font-family: \"Raleway\", sans-serif}
          .topnav {
            overflow: hidden;
            float: right;
            /* background-color: #333; */
          }

          .topnav a {
            float: right;
            display: block;
            /* color: #f2f2f2;
            text-align: center; */
            padding: 7px 13px;
            /* text-decoration: none; */
            /* font-size: 17px; */
          }

          .topnav a:hover {
            /* background-color: #ddd; */
            color: black;
          }

          .topnav a.active {
            /* background-color: #4CAF50; */
            color: white;
          }

          .topnav .icon {
            display: none;
          }

          @media screen and (max-width: 600px) {
            .topnav a:not(:first-child) {display: none;}
            .topnav a.icon {
              float: right;
              display: block;
            }
          }

          @media screen and (max-width: 600px) {
            .topnav.responsive {position: relative;}
            .topnav.responsive .icon {
              position: relative;
              right: 0;
              top: 0;
            }
            .topnav.responsive a {
              float: none;
              display: block;
              text-align: right;
            }
          }
        </style>
    </head>
    <body class=\"w3-light-grey\">
    <!-- Navbar (sit on top) -->
    <div class=\"w3-top\">
      <div class=\"w3-bar w3-white w3-wide w3-padding w3-card\">
        <a href=\"#home\" class=\"w3-bar-item w3-button\"><b>BEYA Joel's</b> Blog</a>
        <!-- Float links to the right. Hide them on small screens -->
        <div class=\"topnav\" id=\"myTopnav\">
          <a href=\"\"></a>
          <a href=\"#projects\" class=\"w3-bar-item w3-button\">Archives</a>
          <a href=\"#about\" class=\"w3-bar-item w3-button\">Mots-clés</a>
          <a href=\"#contact\" class=\"w3-bar-item w3-button\">Contact</a>
          <a href=\"#cv\" class=\"w3-bar-item w3-button\">CV</a>
          <a href=\"javascript:void(0);\" class=\"icon\" onclick=\"myFunction()\">
            <i class=\"fa fa-bars\"></i>
          </a>
          <br>
        </div>
      </div>
    </div>

    <br>
    <br>
    <br>
    <!-- BEGIN w3-content -->
    <div class=\"w3-container\">

    </div>

        ";
        // line 95
        $this->displayBlock('body', $context, $blocks);
        // line 96
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 97
        echo "

    <!-- END w3-content -->
    </div>

    <!-- Footer -->
    <footer class=\"w3-container w3-dark-grey w3-padding-32 w3-margin-top\" style=\"text-align:center; \">
      <button class=\"w3-button w3-black w3-disabled w3-padding-large w3-margin-bottom\">Previous</button>
      <button class=\"w3-button w3-black w3-padding-large w3-margin-bottom\">Next »</button>
      <p>Powered by <a href=\"https://www.w3schools.com/w3css/default.asp\" target=\"_blank\">w3.css</a></p>
    </footer>
    <script>
      function myFunction() {
        var x = document.getElementById(\"myTopnav\");
        if (x.className === \"topnav\") {
          x.className += \" responsive\";
        } else {
          x.className = \"topnav\";
        }
      }
    </script>
    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "BEYA Joel's blog!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "          <link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
          <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Raleway\">
          <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 95
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 96
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  243 => 96,  225 => 95,  212 => 8,  202 => 7,  183 => 6,  150 => 97,  147 => 96,  145 => 95,  60 => 12,  58 => 7,  54 => 6,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>{% block title %}BEYA Joel's blog!{% endblock %}</title>
        {% block stylesheets %}
          <link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
          <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Raleway\">
          <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
        {% endblock %}
        <style>
          body,h1,h2,h3,h4,h5 {font-family: \"Raleway\", sans-serif}
          .topnav {
            overflow: hidden;
            float: right;
            /* background-color: #333; */
          }

          .topnav a {
            float: right;
            display: block;
            /* color: #f2f2f2;
            text-align: center; */
            padding: 7px 13px;
            /* text-decoration: none; */
            /* font-size: 17px; */
          }

          .topnav a:hover {
            /* background-color: #ddd; */
            color: black;
          }

          .topnav a.active {
            /* background-color: #4CAF50; */
            color: white;
          }

          .topnav .icon {
            display: none;
          }

          @media screen and (max-width: 600px) {
            .topnav a:not(:first-child) {display: none;}
            .topnav a.icon {
              float: right;
              display: block;
            }
          }

          @media screen and (max-width: 600px) {
            .topnav.responsive {position: relative;}
            .topnav.responsive .icon {
              position: relative;
              right: 0;
              top: 0;
            }
            .topnav.responsive a {
              float: none;
              display: block;
              text-align: right;
            }
          }
        </style>
    </head>
    <body class=\"w3-light-grey\">
    <!-- Navbar (sit on top) -->
    <div class=\"w3-top\">
      <div class=\"w3-bar w3-white w3-wide w3-padding w3-card\">
        <a href=\"#home\" class=\"w3-bar-item w3-button\"><b>BEYA Joel's</b> Blog</a>
        <!-- Float links to the right. Hide them on small screens -->
        <div class=\"topnav\" id=\"myTopnav\">
          <a href=\"\"></a>
          <a href=\"#projects\" class=\"w3-bar-item w3-button\">Archives</a>
          <a href=\"#about\" class=\"w3-bar-item w3-button\">Mots-clés</a>
          <a href=\"#contact\" class=\"w3-bar-item w3-button\">Contact</a>
          <a href=\"#cv\" class=\"w3-bar-item w3-button\">CV</a>
          <a href=\"javascript:void(0);\" class=\"icon\" onclick=\"myFunction()\">
            <i class=\"fa fa-bars\"></i>
          </a>
          <br>
        </div>
      </div>
    </div>

    <br>
    <br>
    <br>
    <!-- BEGIN w3-content -->
    <div class=\"w3-container\">

    </div>

        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}


    <!-- END w3-content -->
    </div>

    <!-- Footer -->
    <footer class=\"w3-container w3-dark-grey w3-padding-32 w3-margin-top\" style=\"text-align:center; \">
      <button class=\"w3-button w3-black w3-disabled w3-padding-large w3-margin-bottom\">Previous</button>
      <button class=\"w3-button w3-black w3-padding-large w3-margin-bottom\">Next »</button>
      <p>Powered by <a href=\"https://www.w3schools.com/w3css/default.asp\" target=\"_blank\">w3.css</a></p>
    </footer>
    <script>
      function myFunction() {
        var x = document.getElementById(\"myTopnav\");
        if (x.className === \"topnav\") {
          x.className += \" responsive\";
        } else {
          x.className = \"topnav\";
        }
      }
    </script>
    </body>
</html>
", "base.html.twig", "/var/www/public/joel-beya-blog/templates/base.html.twig");
    }
}
